## Sensor de calidad de aire indoor

El sensor MPSensor es un sensor integrado de material particulado (MP), temperatura (T) y humedad relativa (H) que almacena los datos en tiempo real con un módulo SD y un Reloj de Tiempo Real (RTC), desarrollado para el monitoreo de la calidad del aire al interior de la escuela Luis Cruz Martinez de Curacautín, región de la Araucanía.

El Centro de Excelencia en Geotermia de los Andes (CEGA) promueve el uso de la geotermia en diferentes aplicaciones. En la zona centro-sur de Chile resulta particularmente últil en calefacción de ambientes, como reemplazo a los combustibles fósiles que generan altos niveles de contaminación.

Una bomba de calor geotérmica climatiza ambientes utilizando el calor del subsuelo (o agua subterránea) y electricidad. Aproximadamente ¼ de la electricidad respecto de una climatización 100%  eléctrica. Lo que implica un costo operacional similar respecto a la leña, generando una fracción del impacto ambiental. Con el fin de promover la disminución de la contaminación, decidimos calefaccionar mediante el uso de bomba de calor geotérmica una escuela en el pueblo de Curacautín, región de La Araucanía.

Para poder evaluar cuantitativamente los niveles de contaminación presentes en la escuela son necesarios sensores de variables ambientales, en particular de material particulado (MP), temperatura (T) y humedad relativa del aire (RH). En un principio evaluamos la compra de sensores en el mercado local e internacional, sin embargo el costo es muy elevado, por lo que nos incentivamos a construir uno basado en Arduino lo suficientemente preciso como para entregar información relevante respecto de la contaminación de material particulado presente en el Colegio.

![MPsensor](img/mpsensor.jpg)

## Validación del sensor
Equipos estudiados para la comparación:

- Aerocet 531S US$2600 [MetOne](https://metone.com/products/aerocet-531s-handheld-particle-counter/)
- Sharp GP2Y1010AU0F US$12.99 [Amazon](https://www.amazon.com/dp/B07B2PFPB5/ref=sr_1_9_sspa?__mk_es_US=%C3%85M%C3%85%C5%BD%C3%95%C3%91&dchild=1&keywords=sharp+sensor&qid=1599487975&sr=8-9-spons&psc=1&spLa=ZW5jcnlwdGVkUXVhbGlmaWVyPUEyS1dRQkc5Q0NZQVlaJmVuY3J5cHRlZElkPUEwMDc5ODA0VFFYOUVMSlZSME1IJmVuY3J5cHRlZEFkSWQ9QTA3Njg2NzIxMEM4TUFLNTE0UDVSJndpZGdldE5hbWU9c3BfbXRmJmFjdGlvbj1jbGlja1JlZGlyZWN0JmRvTm90TG9nQ2xpY2s9dHJ1ZQ==)
- Sensor nova SDS011 US$36.99 [amazon](https://www.amazon.com/-/es/flashtree-Precision-Quality-Detection-Sensors/dp/B07NSGY7B3/ref=sr_1_3?__mk_es_US=%C3%85M%C3%85%C5%BD%C3%95%C3%91&dchild=1&keywords=Sensor+nova+sds011&qid=1599488294&sr=8-3)

Comparamos tres sensores de material particulado para validar nuestro proyecto. Dos de bajo presupuesto: Sharp GP2Y1010AU0F y sensor Nova SDS011. Este último es el seleccionado. Y uno de alto presupuesto: Aerocet 531Sm el cual entrega 6 tamaños de partículas distintos.

El estudio lo realizamos en dos ambientes: 1) en una oficina con aire acondicionado, es decir, con temperatura y humedad controlados (gráfico lado izquierdo). Y 2) en el exterior, en una terraza techada, con los equipos protegidos de la luz directa y la lluvia (gráfico lado derecho).

Se compararon datos en dos tamaños de material particulado: 2.5 μm y 10 μm. Excepto el sensor Sharp que sólo entrega un tipo de datos mayores a 1 μm.

Resultados en ambiente controlado (gráfico lado izquierdo):
En todos los sensores, ya sea en los tamaños 2,5 μm o 10 μm los valores son similares. Por lo tanto, en ambientes controlados, los sensores de bajo presupuesto como Sensor Nova o Sharp pueden servir en este tipo de ambiente.

Resultados en el exterior (gráfico lado derecho):
En el gráfico se observa que el sensor Sharp se aleja de la forma y tendencia respecto de los demás datos. Si consideramos el resto de los sensores (Aerocet y Nova), los resultados en ambos tamaños de partículas conservan la tendencia, aunque para el tamaño PM 10 muestran mayor dispersión respecto de los de tamaño PM 2.5. De esta manera, los datos de mayor confianza son los de tamaño PM 2.5. Del gráfico se puede concluir que los resultados no son equivalentes entre los sensores Aeroset y Nova, pero muestran resultados satisfactorios para tamaño PM 2.5 y estudios cualitativos. Por lo tanto, recomendamos utilizar los valores de PM 2.5 en estudios cualitativos.

<img src="/img/comparacionsensores.png" width="700">


## Documentación Técnica

### Componentes

|Componente |Precio referencia CLP* | Link proveedor|
--- | --- | ---
|Arduino Pro Mini|$4.950|[Arduino Pro Mini](https://altronics.cl/arduino-pro-mini?search=pro%20mini )|
|RTC DS3231|$4.249|[DS3231](https://altronics.cl/modulo-reloj-tiempo-real-rtc-DS3231-AT24C32?search=reloj)|
|Módulo SD |$1.800  | [SD MOD](https://altronics.cl/modulo-micro-sd-01)|
|Sensor temperatura DHT22 | $4.349 | [DHT22](https://altronics.cl/sensor-temperatura-humedad-dht22?search=dht22) |
|Sensor MP Nova SDS011 |$37.900 |[Nova SDS011](https://evoltapc.cl/sensores/2285-sensor-nova-pm2-5-polvo-alta-presicion-alergia-materiial-particulado.html) |

\* Precios revisados en junio de 2021

### Circuito electrónico
La placa del circuito electrónico integra los componentes para permitir su funcionamiento.

### Código fuente Arduino


# Licencia
[![CC BY SA 4.0][cc-by-sa-image]][cc-by-sa]

Este trabajo esta publicado bajo la licencia [Creative Commons Attribution 4.0 International
License][cc-by-sa].

[cc-by-sa]: https://creativecommons.org/licenses/by-sa/4.0/
[cc-by-sa-image]: https://i.creativecommons.org/l/by-sa/4.0/88x31.png
[cc-by-sa-shield]: https://img.shields.io/badge/License-CC%20BY%20SA%204.0-lightgrey.svg
