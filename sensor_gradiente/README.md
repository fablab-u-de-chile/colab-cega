<!--
![asd](/img/mpsensor.jpg)
<img src="/img/mpsensor.jpg" width="700">
-->
## Sensor gradiente de temperatura


## Documentación Técnica

### Componentes

### Circuito electrico

### Código fuente 

# Licencia
[![CC BY SA 4.0][cc-by-sa-image]][cc-by-sa]

Este trabajo esta publicado bajo la licencia [Creative Commons Attribution 4.0 International
License][cc-by-sa].

[cc-by-sa]: https://creativecommons.org/licenses/by-sa/4.0/
[cc-by-sa-image]: https://i.creativecommons.org/l/by-sa/4.0/88x31.png
[cc-by-sa-shield]: https://img.shields.io/badge/License-CC%20BY%20SA%204.0-lightgrey.svg
