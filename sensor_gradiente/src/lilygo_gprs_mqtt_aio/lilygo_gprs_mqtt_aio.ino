/*
  Rui Santos
  Complete project details at https://RandomNerdTutorials.com/esp32-sim800l-publish-data-to-cloud/

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files.

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
*/

/*
   Este codigo conecta un dispositivo LilyGo-SIM800L-IP5306-20190610
   con Adafruit IO usando una red GPSR y protocolo de datos MQTT
   ref https://github.com/Xinyuan-LilyGO/LilyGo-T-Call-SIM800/blob/master/schematic/LilyGo-SIM800L-IP5306-20190610.pdf

*/

#include "secrets.h"

/* next to "lilygo_gprs_mqtt_aio.ino" in file "secrets.h" you'll need to define: */
/*
  #define AIO_SERVER      "io.adafruit.com"
  #define AIO_SERVERPORT  1883
  #define AIO_USERNAME    // your adafruit io username
  #define AIO_KEY         // your adafruit io key
*/

// for debugging set false
#define PUB_DATA_ONLINE true

// Your GPRS credentials (leave empty, if not needed)
const char apn[]      = "bam.entelpcs.cl"; // APN (example: internet.vodafone.pt) use https://wiki.apnchanger.org
const char gprsUser[] = "entelpcs"; // GPRS User
const char gprsPass[] = "entelpcs"; // GPRS Password

// SIM card PIN (leave empty, if not defined)
const char simPIN[]   = "";

#define uS_TO_S_FACTOR 1000000ULL     /* Conversion factor for micro seconds to seconds */
#define TIME_TO_SLEEP 7200
/*  Time ESP32 will go to sleep (in seconds)
    3600 seconds = 1 hour
    600  seconds = 10 minutes
    60   seconds = 1 minute
*/

// Set serial for debug console (to Serial Monitor, default speed 115200)
#define SerialMon Serial
// Set serial for AT commands (to SIM800 module)
#define SerialAT Serial1
// Set software serial name for OpenLog
#define OpenLog Serial2

#define DEBUG_LED 13
#define HW_POWER_SWITCH 0
#define BATT_MONITOR_PIN 34 //(ADC1_CH6)
#define NUM_SAMPLES 10

#define OL_BAUDRATE 9600 // open log serial comm baudrate
#define TIMEOUT_SD 20000
#define TIMEOUT_LOG 10000

#include <Adafruit_MQTT.h>
#include <Adafruit_MQTT_Client.h>
#include <RTClib.h>
#include <Wire.h>
#include <Adafruit_Sensor.h>
#include <SparkFun_TMP117.h>

// TTGO T-Call pins
#define MODEM_RST            5
#define MODEM_PWKEY          4
#define MODEM_POWER_ON       23
#define MODEM_TX             27
#define MODEM_RX             26

// I2C 1st port pins
#define I2C_SDA              21
#define I2C_SCL              22
// I2C 2nd port pins
#define I2C_SDA_2            18
#define I2C_SCL_2            19

TwoWire I2C_P1 = TwoWire(0);
TMP117 tmp1; // p1-1 dir 0x48
TMP117 tmp3; // p1-2 dir 0x49
TMP117 tmp5; // p1-3 dir 0x4A
TMP117 tmp7; // p1-4 dir 0x4B

#define TMP1_ADDR 0x48
#define TMP3_ADDR 0x49
#define TMP5_ADDR 0x4A
#define TMP7_ADDR 0x4B

// I2C P2
TwoWire I2C_P2 = TwoWire(1);
TMP117 tmp2; // p2-1 dir 0x48
TMP117 tmp4; // p2-2 dir 0x49
TMP117 tmp6; // p2-3 dir 0x4A
TMP117 tmp8; // p2-4 dir 0x4B

#define TMP2_ADDR 0x48
#define TMP4_ADDR 0x49
#define TMP6_ADDR 0x4A
#define TMP8_ADDR 0x4B

// total number of sensors connected
const int numTMP = 8;

TMP117 *tmps[numTMP] = {&tmp1, &tmp2, &tmp3, &tmp4, &tmp5, &tmp6, &tmp7, &tmp8};
int sensors[numTMP] = {1, 2, 3, 4, 5, 6, 7, 8};
int addrs[numTMP] = {TMP1_ADDR, TMP2_ADDR, TMP3_ADDR, TMP4_ADDR, TMP5_ADDR, TMP6_ADDR, TMP7_ADDR, TMP8_ADDR};

// for temperature values
float temp[numTMP];

char mesg[20] = "";
char ol_buffer[30] = "";

// Configure TinyGSM library
#define TINY_GSM_MODEM_SIM800      // Modem is SIM800
#define TINY_GSM_RX_BUFFER   1024  // Set RX buffer to 1Kb

// Define the serial console for debug prints, if needed
//#define DUMP_AT_COMMANDS

#include <TinyGsmClient.h>

#ifdef DUMP_AT_COMMANDS
#include <StreamDebugger.h>
StreamDebugger debugger(SerialAT, SerialMon);
TinyGsm modem(debugger);
#else
TinyGsm modem(SerialAT);
#endif

// TinyGSM Client for Internet connection
TinyGsmClient client(modem);

Adafruit_MQTT_Client mqtt(&client, AIO_SERVER, AIO_SERVERPORT, AIO_USERNAME, AIO_KEY);
Adafruit_MQTT_Publish t1 = Adafruit_MQTT_Publish(&mqtt, AIO_USERNAME "/feeds/tmp117.t1");
Adafruit_MQTT_Publish t2 = Adafruit_MQTT_Publish(&mqtt, AIO_USERNAME "/feeds/tmp117.t2");
Adafruit_MQTT_Publish t3 = Adafruit_MQTT_Publish(&mqtt, AIO_USERNAME "/feeds/tmp117.t3");
Adafruit_MQTT_Publish t4 = Adafruit_MQTT_Publish(&mqtt, AIO_USERNAME "/feeds/tmp117.t4");
Adafruit_MQTT_Publish t5 = Adafruit_MQTT_Publish(&mqtt, AIO_USERNAME "/feeds/tmp117.t5");
Adafruit_MQTT_Publish t6 = Adafruit_MQTT_Publish(&mqtt, AIO_USERNAME "/feeds/tmp117.t6");
Adafruit_MQTT_Publish t7 = Adafruit_MQTT_Publish(&mqtt, AIO_USERNAME "/feeds/tmp117.t7");
Adafruit_MQTT_Publish t8 = Adafruit_MQTT_Publish(&mqtt, AIO_USERNAME "/feeds/tmp117.t8");

Adafruit_MQTT_Publish VBAT = Adafruit_MQTT_Publish(&mqtt, AIO_USERNAME "/feeds/tmp117.vbat");

Adafruit_MQTT_Publish *pubs[numTMP] = {&t1, &t2, &t3, &t4, &t5, &t6, &t7, &t8};

#define IP5306_ADDR          0x75
#define IP5306_REG_SYS_CTL0  0x00

// redefine los pines del uart1
// los que vienen por defecto en el esp32 (9,10) no se pueden utilizar para uart externo
#define OL_RX 2
#define OL_TX 15
// caracter de salida, configurado en el archivo config.txt almacenado en la tarjeta micro SD
#define OL_EXIT_CHAR  26
// pin para reiniciar el OpenLog conectado al pin GRN
#define OL_RESET_PIN  4

RTC_DS3231 RTC; // define the Real Time Clock object, connect only to default I2C port (21,22)

int batteryLevel = 0;

bool setPowerBoostKeepOn(int en) {
  I2C_P1.beginTransmission(IP5306_ADDR);
  I2C_P1.write(IP5306_REG_SYS_CTL0);
  if (en) {
    I2C_P1.write(0x37); // Set bit1: 1 enable 0 disable boost keep on
  } else {
    I2C_P1.write(0x35); // 0x37 is default reg value
  }
  return I2C_P1.endTransmission() == 0;
}

void setup() {

  pinMode(HW_POWER_SWITCH, OUTPUT);
  digitalWrite(HW_POWER_SWITCH, LOW);

  pinMode(DEBUG_LED, OUTPUT);
  blinkLed(DEBUG_LED, 1);

  // Set serial monitor debugging window baud rate to 115200
  SerialMon.begin(115200);

  //SerialMon.println("OpenLog software serial port");
  OpenLog.begin(OL_BAUDRATE, SERIAL_8N1, OL_RX, OL_TX); //Open software serial port at 9600bps

  // Start I2C communication
  I2C_P1.begin(I2C_SDA, I2C_SCL, 400000);
  I2C_P2.begin(I2C_SDA_2, I2C_SCL_2, 400000);

  // Keep power when running from battery
  bool isOk = setPowerBoostKeepOn(1);
  SerialMon.println(String("IP5306 KeepOn ") + (isOk ? "OK" : "FAIL"));

  // Set modem reset, enable, power pins
  pinMode(MODEM_PWKEY, OUTPUT);
  pinMode(MODEM_RST, OUTPUT);
  pinMode(MODEM_POWER_ON, OUTPUT);
  digitalWrite(MODEM_PWKEY, LOW);
  digitalWrite(MODEM_RST, HIGH);
  digitalWrite(MODEM_POWER_ON, HIGH);

  // Set GSM module baud rate and UART pins
  SerialAT.begin(115200, SERIAL_8N1, MODEM_RX, MODEM_TX);
  delay(3000);

  // Restart SIM800 module, it takes quite some time
  // To skip it, call init() instead of restart()
  SerialMon.print("Initializing modem... ");
  modem.restart();
  SerialMon.println("OK");
  // use modem.init() if you don't need the complete restart

  // Unlock your SIM card with a PIN if needed
  if (strlen(simPIN) && modem.getSimStatus() != 3 ) {
    modem.simUnlock(simPIN);
  }

  // inicializa RTC y sensores de temp
  setupHW();

  // Configure the wake up source as timer wake up
  esp_sleep_enable_timer_wakeup(TIME_TO_SLEEP * uS_TO_S_FACTOR);
  resetOpenLog();
}

void loop() {
  DateTime now = RTC.now();

  // get battery voltage
  batteryLevel = getBatteryVoltage();
  Serial.println("Vbat: " + String(batteryLevel));

  // get data from sensors
  getSensorData();

#if PUB_DATA_ONLINE == true
  if (APN_connect() == true) {

    if (MQTT_connect() == true)
      pubData();  // publica datos de los sensores y voltaje de la bateria

    // Close client and disconnect
    client.stop();
    SerialMon.println(F("Server disconnected"));
    modem.gprsDisconnect();
    SerialMon.println(F("GPRS disconnected"));
  }
#endif

  // prepare buffer for logging or debugging
  sprintf(ol_buffer, "%d;%d;%.2f;%.2f;%.2f;%.2f;%.2f;%.2f;%.2f;%.2f", now.unixtime(), batteryLevel, temp[0], temp[1], temp[2], temp[3], temp[4], temp[5], temp[6], temp[7]);
  Serial.println(ol_buffer);
  // guarda string de datos en tajeta SD
  logData(now);

  // apaga hardware externo
  turnOffHW();

  // Put ESP32 into deep sleep mode (with timer wake up)
  Serial.println("going to deep sleep mode...");
  esp_deep_sleep_start();
}

void turnOffHW() {
  /*
     configura todos los pines digitales en LOW
     apaga el transistor que alimenta el Hardware externo al esp32
  */
  digitalWrite(HW_POWER_SWITCH, LOW);
}

void getSensorData() {
  // geta data from sensors
  for (int i = 0; i < numTMP; i++) {
    if (tmps[i]->dataReady() == true) {
      temp[i] = tmps[i]->readTempC();
#if PUB_DATA_ONLINE == false
      sprintf(mesg, "temp sensor t%d: ", sensors[i]);
      Serial.print(mesg);
      Serial.println(temp[i], 2);
#endif
    }
  }
}

void logData(DateTime now) {
  OpenLog.println(ol_buffer);
}

void pubData() {
  for (int i = 0; i < numTMP; i++) {
    sprintf(mesg, "temp sensor t%d: ", sensors[i]);
    Serial.print(mesg);
    Serial.print(temp[i], 2);
    Serial.print(" ... ");
    if (!pubs[i]->publish(temp[i])) {
      Serial.println(F("pub Failed"));
    } else {
      Serial.println(F("pub OK!"));
    }
    delay(200);
  }
  delay(500);
  // publica voltaje de la bateria
  VBAT.publish(batteryLevel);
  delay(500);
}

bool APN_connect()
{
  SerialMon.print("Connecting to APN: ");
  SerialMon.print(apn);
  if (!modem.gprsConnect(apn, gprsUser, gprsPass)) {
    blinkLed(DEBUG_LED, 10);
    SerialMon.println(" fail");
    return false;
  }
  else
  {
    blinkLed(DEBUG_LED, 2);
    SerialMon.println(" OK");
    return true;
  }
}

bool MQTT_connect()
{
  int8_t ret;

  // Stop if already connected.
  if (mqtt.connected()) {
    return true;
  }

  Serial.print("Connecting to MQTT... ");

  uint8_t retries = 5;

  while ((ret = mqtt.connect()) != 0) { // connect will return 0 for connected
    Serial.println(mqtt.connectErrorString(ret));
    Serial.print("Retrying MQTT connection in 5 seconds... retries left: ");
    Serial.print(retries);
    Serial.print("... ");
    mqtt.disconnect();

    delay(5000); // wait 5 seconds

    retries--;
    if (retries == 0) {
      // basically die and wait for WDT to reset me
      // while (1);
      Serial.println(mqtt.connectErrorString(ret));
      return false;
    }
  }
  Serial.println("MQTT Connected!");
  return true;
}

void setupHW() {
  digitalWrite(HW_POWER_SWITCH, HIGH);
  delay(500);

  if (!RTC.begin()) {
    SerialMon.println("RTC failed");
  }

  for (int i = 0; i < numTMP; i++) {
    if (i % 2 == 0) {
      if (tmps[i]->begin(addrs[i], I2C_P1) == true) {
        sprintf(mesg, "begin sensor: %d, at addr: 0x%X", sensors[i], addrs[i]);
        //sprintf(mesg, "begin sensor %d", i + 1);
        SerialMon.println(mesg);
      }
      else {
        sprintf(mesg, "failed sensor: %d, at addr: 0x%X", sensors[i], addrs[i]);
        SerialMon.println(mesg);
        //while (1); // Runs forever
      }
    }
    else {
      if (tmps[i]->begin(addrs[i], I2C_P2) == true) {
        sprintf(mesg, "begin sensor: %d, at addr: 0x%X", sensors[i], addrs[i]);
        //sprintf(mesg, "begin sensor %d", i + 1);
        SerialMon.println(mesg);
      }
      else {
        sprintf(mesg, "failed sensor: %d, at addr: 0x%X", sensors[i], addrs[i]);
        SerialMon.println(mesg);
        //while (1); // Runs forever
      }
    }
  }
}

/*
  int getBatteryVoltage()
  {
  int batReading = analogRead(BATT_MONITOR_PIN);
  float vBat = batReading * 3.35 / 4096.0 * 2;
  int vBatI = vBat * 1000;
  //Serial.println(vBat);
  //Serial.println(vBatI);
  return vBatI;
  }
*/

int getBatteryVoltage() {
  int batReading = 0;
  int num_samples = NUM_SAMPLES;
  // take analog samples
  for (int i = 0; i < num_samples; i++) {
    batReading += analogRead(BATT_MONITOR_PIN);
    delay(1);
  }
  float vBat = (batReading / num_samples) * 3.35 / 4096.0 * 2;
  int vBatI = vBat * 1000;
  //Serial.println(vBat);
  //Serial.println(vBatI);
  return vBatI;
}


void resetOpenLog() {
  digitalWrite(OL_RESET_PIN, LOW);
  digitalWrite(DEBUG_LED, HIGH);
  delay(500);
  digitalWrite(OL_RESET_PIN, HIGH);
  digitalWrite(DEBUG_LED, LOW);
  delay(500);
}

void blinkLed(int LED, int T)
{
  for (int i = 0; i < T; i++)
  {
    digitalWrite(LED, HIGH);
    delay(50);
    digitalWrite(LED, LOW);
    delay(50);
  }
}
