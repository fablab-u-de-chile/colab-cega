# colab-cega

Proyectos de la colaboración con el Centro de Excelencia en Geotermia de Los Andes

## Sensor de calidad de aire indoor

El sensor MPSensor es un sensor integrado de material particulado (MP), temperatura (T) y humedad relativa (H) que almacena los datos en tiempo real con un módulo SD y un Reloj de Tiempo Real (RTC), desarrollado para el monitoreo de la calidad del aire al interior de la escuela Luis Cruz Martinez de Curacautín, región de la Araucanía.

Puedes revisar la documentación completa del sensor [acá][mpsensor]


## Sensor gradiente de temperatura volcanes

Puedes revisar la documentación completa del sensor [acá][gradsensor]

[mpsensor]: /sensor_ambiental
[gradsensor]: /sensor_gradiente


# Licencia
[![CC BY SA 4.0][cc-by-sa-image]][cc-by-sa]

Este trabajo esta publicado bajo la licencia [Creative Commons Attribution 4.0 International
License][cc-by-sa].

[cc-by-sa]: https://creativecommons.org/licenses/by-sa/4.0/
[cc-by-sa-image]: https://i.creativecommons.org/l/by-sa/4.0/88x31.png
[cc-by-sa-shield]: https://img.shields.io/badge/License-CC%20BY%20SA%204.0-lightgrey.svg
